#include "bot.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>

double myx;
double myy;
int mymass;
int mysize;

//distance of other players relative to bot
int distplayer(const void *a, const void *b)
{
    double ax = (*(struct player *)a).x;
    double ay = (*(struct player *)a).y;
    double bx = (*(struct player *)b).x;
    double by = (*(struct player *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

//distance of food relative to bot
int distfood(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

//distance of cellls relative to bot
int distvirii(const void *a, const void *b)
{
    double ax = (*(struct cell *)a).x;
    double ay = (*(struct cell *)a).y;
    double bx = (*(struct cell *)b).x;
    double by = (*(struct cell *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    // Copy player's x-y to globals
    myx = me.x;
    myy = me.y;
    mymass = me.totalMass;
    
    // Sort food by distance
    qsort(foods, nfood, sizeof(struct food), distfood);
   
    // Sort players by distance
    qsort(players, nplayers, sizeof(struct player), distplayer);
   
    //sort virus by distance
    qsort(virii, nvirii, sizeof(struct cell), distvirii);
    
    act.fire = 0;
    act.split = 0;
   
    //check for players
    if(nplayers > 0)
    {
        //check if close
        if(((players[0].x - me.x)<=250)||((players[0].y - me.y)<=250))
        {
            //check if larger
            if(players[0].totalMass > mymass)
            {
                //move away from nearest player if smaller
                act.dx = (players[0].x - me.x)*-1000;
                act.dy = (players[0].y - me.y)*-1000;
                return act;
            }
            else
            {
                //move towards nearest player
                act.dx = (players[0].x - me.x)*1000;
                act.dy = (players[0].y - me.y)*1000;
                return act;
            }
        }
    }
    //avoid virii
    if(sqrt((myx-virii[0].x)*(myx-virii[0].x)+(myy - virii[0].y)*(myy - virii[0].y))<50)
    {
        act.dx = (virii[0].x - me.x)*-1000;
        act.dy = (virii[0].y - me.y)*-1000;
        return act;
    }
    if(nfood > 0)
    {
        // Move toward closest food
        act.dx = (foods[0].x - me.x)*1000;
        act.dy = (foods[0].y - me.y)*1000;
        return act;
    }
    else
    {
        //just move if nothing near by
        act.dx = (me.x*rand()*100);
        act.dy = (me.y*rand()*100);
        return act;
    }
}